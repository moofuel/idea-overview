FROM openjdk:8u131-jre
LABEL "Description"="Intellij IDEA funtionality overview"
COPY target/*.jar /opt/ideaoverview/idea.jar
WORKDIR /opt/ideaoverview
EXPOSE 8080 5005
ENTRYPOINT ["java", "-jar", "idea.jar"]