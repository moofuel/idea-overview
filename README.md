[![pipeline status](https://gitlab.com/moofuel/idea-overview/badges/master/pipeline.svg)](https://gitlab.com/moofuel/idea-overview/commits/master)[![coverage report](https://gitlab.com/moofuel/idea-overview/badges/master/coverage.svg)](https://gitlab.com/moofuel/idea-overview/commits/master)
<div align="center"><H1>Проект-презентация "Работа в IDEA"</H1></div>

Must-have плагины
-------------------
* .ignore - плагин для работы с файлами .gitignore
* docker - плагин для интеграции с Docker
* sonarlint - плагин для статического анализа кода
* FindBugs-IDEA - плагин для статического анализа кода
* Lombok - плагин для интеграции с Lombok
* Key Promoter X - если используется мышь для навигации - подсказывает каким шорткатом это можно было сделать


Docker Integration
-------------------
* https://plugins.jetbrains.com/plugin/7724-docker-integration - ссылка для загрузки плагина(можно установить из репозитория плагинов внутри самой IDEA)
* https://www.jetbrains.com/help/idea/docker.html
* https://www.jetbrains.com/help/idea/run-debug-configuration-docker-deployment.html

Видео:
-------------------

* https://www.youtube.com/watch?v=eq3KiAH4IBI - доклад о возможностях IDEA на английском языке
* https://www.youtube.com/watch?v=_rj7dx6c5R8 - доклад о возможностях IDEA на русском языке

