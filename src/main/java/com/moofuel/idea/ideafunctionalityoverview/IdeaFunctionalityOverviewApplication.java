package com.moofuel.idea.ideafunctionalityoverview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdeaFunctionalityOverviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(IdeaFunctionalityOverviewApplication.class, args);
    }
}
