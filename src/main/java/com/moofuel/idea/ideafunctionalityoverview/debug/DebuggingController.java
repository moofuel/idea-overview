package com.moofuel.idea.ideafunctionalityoverview.debug;

import org.springframework.web.bind.annotation.*;

/**
 * @author Дмитрий
 * @since 03.11.2017
 */
@RestController
@RequestMapping("/debug")
public class DebuggingController {


    @GetMapping("/{id}")
    public String debug(@PathVariable String id,
                        @RequestParam String name) {
        final String result = this.debug1(id, name);
        System.out.println(result);
        return result;
    }

    private String debug1(String id, String name) {
        return this.debug2(id, name, true, "message");
    }

    private String debug2(String id, String name, boolean debug, String message) {
        for (int i = 0; i < 5; i++) {
            System.out.println("шаг в цикле: " + i);
        }
        return message;
    }
}
