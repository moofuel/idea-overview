package com.moofuel.idea.ideafunctionalityoverview.debug;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Дмитрий
 * @since 31.10.2017
 */
@RestController
@RequestMapping
public class DebugInDockerController {

    @GetMapping
    public String hello() {
        System.out.println("TestController.hello");
        return "{\"kek\":\"lol\"}";
    }
}
